## CAR LOAN CALCULATOR

**Car Loan Calculator** is a digital computation system that allows a borrower to project how much the monthly payment will be on a loan and how long it will take to repay the borrowed amount.

This is an android app developed with Ionic 4 Framework, its features are the following:

1. Loan Computation
2. Detailed Amortization
3. Auto Calculate per input
4. PDF Print
5. Profile Page
6. Native Facebook Login
7. Native Google Login
8. Email Sign in / Sign up
9. Firebase Integrated

*by* **Key Innovations Inc.** and
*developed by* **Lovella June Gonzaga** 

# INSTRUCTIONS
1. Clone repo
2. npm install
3. ionic serve -l