import { Component, ViewChild } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { IonSlides } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Storage } from "@ionic/storage";

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import * as moment from 'moment';

@Component({
  selector: 'app-calculate',
  templateUrl: './calculate.page.html',
  styleUrls: ['./calculate.page.scss'],
})
export class CalculatePage{

  pdfObj = null;

  @ViewChild('slides') slider: IonSlides;

  segment;

  clicksegment: string;

  loanAmount: number;
  monthlyPayment;
  totalNumberOfPayments;
  startDate;
  payOffDate;
  downPayment: number;
  totalInterestPaid: number;
  totalPayment;
  totalOfAllCosts: number;

  customInterestRate;
  customLoanTerm;
  customDownPayment;
  displayRadio;

  loading: any;
  vehicleValue: any;
  loanTerm: any;
  interestRate: any;
  power: any;
  newInterestRate;
  multiplier: any;

  get_date=new Date();

  displayTotal;
  displayPrincipal;
  displayInterest;
  displayBalance;
  displayMonth;

  rows = [];

  constructor(private fireAuth: AngularFireAuth, public loadingController: LoadingController,
    private file: File,
    private fileOpener: FileOpener,
    public storage: Storage,
    private platform: Platform,
    private router: Router) {
      
      this.customLoanTerm = "month";
      this.customDownPayment = "php";
      this.customInterestRate = "yearly";

      // this.vehicleValue = 0
      // this.downPayment = 0;
      // this.loanTerm = 0;
      // this.interestRate = 0;
      // this.loanAmount = 0;

      // this.monthlyPayment = 0;
      // this.totalInterestPaid = 0;
      // this.totalOfAllCosts = 0;
      // this.totalPayment = 0;
    }

    ionViewWillLoad(){
      this.segment = "0";
    }
    
    onSelectChange(){
        this.loanTerm = this.loanTerm * 12;
        this.totalNumberOfPayments = this.loanTerm;
    }
  
    onClickLoanChange(){
      this.totalNumberOfPayments = this.loanTerm;
    }

    dpOnDetails;
  
    onDownSetting(){
      if ( this.customDownPayment == "percent" ){
        this.downPayment = (this.downPayment*100) / this.vehicleValue;
        this.dpOnDetails = (this.downPayment/100)*this.vehicleValue;
      }
      if ( this.customDownPayment == "php" ){
        this.downPayment = (this.downPayment/100) * this.vehicleValue;
        //console.log(this.downPayment);
      }
    }

    onDownDetails(){
      if (this.downPayment <= 100){
        this.dpOnDetails = (this.downPayment/100)*this.vehicleValue;
      }
      if ( this.customDownPayment == "percent" ){
        this.dpOnDetails = (this.downPayment/100)*this.vehicleValue;
      }
      if ( this.customDownPayment == "php" ){
        this.dpOnDetails = this.downPayment;
      }
    } 

    send_date=new Date();
  
    onSetDate(){
        
        var checker = this.startDate.split("-");
        var month_index = parseInt(checker[1],10)+this.loanTerm-1;
        var date_index = parseInt(checker[2],10);
        var year_index = parseInt(checker[0],10);

        // console.log(month_index);
        // console.log(date_index);
        // console.log(year_index);

        var subtract = month_index-12;

        // console.log(subtract);

        const start = month_index-subtract + "/" + date_index + "/" + year_index;
        const mstart = moment(start, "MM/DD/YYYY").add(subtract, 'M');
        const mmstart = mstart.add(1, 'd');

        // console.log(start);
        // console.log(mstart);
        // console.log(mmstart);

        var datetest = moment.utc(mmstart).calendar();

        // console.log(datetest);
        this.payOffDate = datetest;

        //console.log(datetest);
    }
  
    onClickChange(){
      if ( this.customDownPayment == "php" ){
        this.loanAmount = this.vehicleValue - this.downPayment;
      }
      if ( this.customDownPayment == "percent" ){
        this.loanAmount = this.vehicleValue - ((this.downPayment/100)*this.vehicleValue);
      }
        this.newInterestRate = this.interestRate/100/12;
        this.power = Math.pow((1 + this.newInterestRate),this.loanTerm);
        this.multiplier = 1+(this.newInterestRate / 100);
        this.monthlyPayment = (this.loanAmount * this.newInterestRate * this.power) / (this.power - 1) * 1.00;
        this.monthlyPayment = this.monthlyPayment.toFixed(2);
        this.totalNumberOfPayments = this.loanTerm;
        this.totalPayment = this.monthlyPayment * this.loanTerm;
        this.totalPayment = this.totalPayment.toFixed(2);
        this.totalInterestPaid = (this.monthlyPayment * this.loanTerm) - this.loanAmount;
        this.totalOfAllCosts = this.loanAmount + this.downPayment + this.totalInterestPaid;
    }
  
    tableInterestRate;
    yearlyTotalNumberOfPayments;
    monthlyTotalNumberOfPayments;
    dateInc;
    tableDate;
  
    displaySegment (){
      if ( this.totalNumberOfPayments < 6){
        this.monthlyTotalNumberOfPayments = this.totalNumberOfPayments*12;
      }
  
      if ( this.totalNumberOfPayments > 6){
        this.monthlyTotalNumberOfPayments = this.totalNumberOfPayments;
      }

      const month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
      this.displayTotal = this.monthlyPayment;
      this.tableInterestRate = this.interestRate/100;
      this.displayInterest = (this.tableInterestRate/12)*this.loanAmount;
      this.displayPrincipal = this.displayTotal - this.displayInterest;
      this.displayBalance = this.loanAmount;
      var formatted_date = month[this.get_date.getMonth()] + "/" + this.get_date.getFullYear();
      this.tableDate = formatted_date;

      var jsonObject = {};
  
      jsonObject['date'] = this.tableDate;
      jsonObject['interest'] = this.displayInterest;
      jsonObject['principal'] = this.displayPrincipal;
      jsonObject['total'] = this.displayTotal;
      jsonObject['balance'] = this.displayBalance;
  
      var last = new Array();

      var checker2 = this.startDate.split("-");
      var month_index2 = parseInt(checker2[1],10);
      var date_index2 = parseInt(checker2[2],10);
      var year_index2 = parseInt(checker2[0],10);

      const start2 = month_index2 + "/" + date_index2 + "/" + year_index2;
      const mstart2 = moment(start2, "MM/DD/YYYY");
      const mmstart2 = mstart2.add(1, 'd');
      var datetable = moment.utc(mmstart2).calendar();

      var mm = month_index2-1;
      var yyyy = year_index2;     
      var dd = date_index2;

      for (var i = 0; i <= this.monthlyTotalNumberOfPayments - 1; i++) {
        var copy = JSON.parse(JSON.stringify(jsonObject));
        
        if(mm > 12){
          yyyy= yyyy+1;
          mm = mm-12;
        }
        copy['date'] = month[mm] + "/" + dd + "/" + yyyy;

        mm = mm+1;

        if (mm == 13){
          mm = mm-12;
          yyyy = yyyy+1;
          copy['date'] = month[mm-1] + "/" + dd + "/" + yyyy;
        }

        copy['interest'] = (this.tableInterestRate/12) * this.displayBalance;
        this.displayInterest = (this.tableInterestRate/12) * this.displayBalance;
        this.displayInterest = this.displayInterest.toFixed(2);
        copy['principal'] = this.displayTotal - this.displayInterest;
        this.displayPrincipal = this.displayTotal - this.displayInterest;
        this.displayPrincipal = this.displayPrincipal.toFixed(2);
        copy['balance'] = this.displayBalance - this.displayPrincipal;
        this.displayBalance = this.displayBalance - this.displayPrincipal;
        this.displayBalance = this.displayBalance.toFixed(2);
        if(! (this.displayBalance > 1) ){
          copy['balance'] = 0;
        }
        if (this.displayBalance < 1){
          this.displayBalance = 0;
        }
        last.push(copy);
      }
    
        this.storage.set('my-json', last);
    
        this.storage.get('my-json').then((val) => {
          this.rows = val.splice(0,100);
        });
    }

    async presentLoading(msg) {
      this.loading = await this.loadingController.create({
        message: msg
      });
      return await this.loading.present();
    }

    sDate: any;
    pDate: any;

    print_date = new Date();

    printdate = this.print_date.getMonth() + "/" + this.print_date.getDate() + "/" + this.print_date.getFullYear();

    createPdf() {

    var pdf_date = new Date();
    var pdf_endate = new Date();

    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    this.sDate = monthNames[pdf_date.getMonth()] + " " + pdf_date.getDate() + "," + " " + pdf_date.getFullYear();
    pdf_endate.setMonth(pdf_endate.getMonth()+this.loanTerm-1);
    this.pDate = monthNames[pdf_endate.getMonth()] + " " + pdf_endate.getDate() + "," + " " + pdf_endate.getFullYear();
    
    const month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  
      this.displayTotal = this.monthlyPayment;
      this.tableInterestRate = this.interestRate/100;
      this.displayInterest = (this.tableInterestRate/12)*this.loanAmount;
      this.displayPrincipal = this.displayTotal - this.displayInterest;
      this.displayBalance = this.loanAmount;
      var formatted_date = month[this.get_date.getMonth()] + "/" + this.get_date.getFullYear();
      this.tableDate = formatted_date;

      var jsonObject = {};
  
      jsonObject['date'] = this.tableDate;
      jsonObject['interest'] = this.displayInterest;
      jsonObject['principal'] = this.displayPrincipal;
      jsonObject['total'] = this.displayTotal;
      jsonObject['balance'] = this.displayBalance;
  
      var last = new Array();

      var checker2 = this.startDate.split("-");
      var month_index2 = parseInt(checker2[1],10);
      var date_index2 = parseInt(checker2[2],10);
      var year_index2 = parseInt(checker2[0],10);

      const start2 = month_index2 + "/" + date_index2 + "/" + year_index2;
      const mstart2 = moment(start2, "MM/DD/YYYY");
      const mmstart2 = mstart2.add(1, 'd');
      var datetable = moment.utc(mmstart2).calendar();

      var mm = month_index2-1;
      var yyyy = year_index2;
      var dd = date_index2;
  
      for (var i = 0; i <= this.monthlyTotalNumberOfPayments - 1; i++) {
        var copy = JSON.parse(JSON.stringify(jsonObject));
        
        if(mm > 12){
          yyyy= yyyy+1;
          mm = mm-12;
        }
        copy['date'] = month[mm] + "/"  + dd + "/" + yyyy;
        mm = mm+1;

        if (mm == 13){
          mm = mm-12;
          yyyy = yyyy+1;
          copy['date'] = month[mm-1] + "/" + dd + "/" + yyyy;
        }

        copy['interest'] = (this.tableInterestRate/12) * this.displayBalance;
        this.displayInterest = (this.tableInterestRate/12) * this.displayBalance;
        this.displayInterest = this.displayInterest.toFixed(2);
        copy['principal'] = this.displayTotal - this.displayInterest;
        this.displayPrincipal = this.displayTotal - this.displayInterest;
        this.displayPrincipal = this.displayPrincipal.toFixed(2);
        copy['balance'] = this.displayBalance - this.displayPrincipal;
        this.displayBalance = this.displayBalance - this.displayPrincipal;
        this.displayBalance = this.displayBalance.toFixed(2);
        if(! (this.displayBalance > 1) ){
          copy['balance'] = 0;
        }
        if (this.displayBalance < 1){
          this.displayBalance = 0;
        }
        last.push(copy);
      }
    
        this.storage.set('my-json', last);
    
        this.storage.get('my-json').then((val) => {
          this.rows = val.splice(0,100);
        });

        function formatNumber(num){
          return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }

        var tabs = [];
        var rows = JSON.parse(JSON.stringify(last));
        tabs.push(['DUE DATE','INTEREST','PRINCIPAL','TOTAL AMT DUE','RUNNING BALANCE']);

        for(var x in rows) {
            if ( rows[x].balance < 1 ){
              rows[x].balance = 0;
            }
            tabs.push([rows[x].date, '₱ ' + formatNumber(rows[x].interest.toFixed(2)), '₱ ' + formatNumber(rows[x].principal.toFixed(2)), '₱ ' + formatNumber(rows[x].total), '₱ ' + formatNumber(rows[x].balance.toFixed(2))]);
        }

      var docDefinition = {
        content: [
          'LOAN AMORTIZATION DETAILS\n',
          {
            columns: [
              {
                text: '\nPrincipal\n₱ ' + formatNumber(this.loanAmount) + '\n\nDown Payment\n₱ ' + formatNumber(this.dpOnDetails) + '\n\nMonthly Payment\n₱ ' + formatNumber(this.monthlyPayment),
              },
              {
                text: '\nTotal Number of Payments\n₱ ' + formatNumber(this.totalNumberOfPayments) + '\n\nStart Date\n' + this.sDate + '\n\nEnd Date\n' + this.pDate,
              },
              {
                text: '\nTotal Interest\n₱ ' + formatNumber(this.totalInterestPaid.toFixed(2)) + '\n\nTotal Amount Due\n₱ ' + formatNumber(this.totalPayment) + '\n\nTotal Revenue\n₱ ' + formatNumber(this.totalOfAllCosts.toFixed(2)),
              }
            ],
            columnGap: 10
          },
          // { text: this.printdate, style: 'datesize' },
          { text: '\n' },
          {
                    table: {
                      widths: [85, 85, 80, 100, 120],
                      body: tabs
                  }
                }
              ],
              styles: {
                header: {
                  fontSize: 16,
                  bold: true,
                },
                subheader: {
                  fontSize: 12,
                  bold: true,
                  margin: [0, 13, 0, 0]
                },
                datesize: {
                  fontSize: 14,
                  bold: true,
                  margin: [0, 13, 0, 0],
                  lineHeight: 1.2
                }
              }
            }
            this.pdfObj = pdfMake.createPdf(docDefinition);

      if (this.platform.is('cordova')) {
        this.pdfObj.getBuffer((buffer) => {
          var blob = new Blob([buffer], { type: 'application/pdf' });

          this.file.writeFile(this.file.externalApplicationStorageDirectory, 'loan_amortization.pdf', blob, { replace: true }).then(fileEntry => {
            this.fileOpener.open(this.file.externalApplicationStorageDirectory + 'loan_amortization.pdf', 'application/pdf');

          })
        });
      } else {
        this.pdfObj.download();
      }
    }

  async ngOnInit() {
  	this.loading = await this.loadingController.create({
      message: 'Connecting ...'
    });
  }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  logout() {
    this.fireAuth.auth.signOut().then(() => {
      this.router.navigate(["/home"]);
    })
  }

}
