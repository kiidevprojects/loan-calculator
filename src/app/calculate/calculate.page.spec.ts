import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatePage } from './calculate.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

describe('CalculatePage', () => {
  let component: CalculatePage;
  let fixture: ComponentFixture<CalculatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculatePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ NgxDatatableModule ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
