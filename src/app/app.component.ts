import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  rootPage:any = 'TablePage';
  navigate: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private fireAuth: AngularFireAuth
  ) {
    this.sideMenu();
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.fireAuth.auth.onAuthStateChanged(user => {
        if (user) {
          this.router.navigate(["/calculate"]);
          this.splashScreen.hide();
        }
        else {
          this.router.navigate(["/home"]);
          this.splashScreen.hide();
        }
      })
      this.statusBar.styleDefault();
    });
  }

  sideMenu(){
    this.navigate =
    [
      {
        title: "Loan Calculator",
        url: "/calculate",
        icon: "calculator"
      },
      {
        title: "Profile",
        url: "/profile",
        icon: "people"
      },
      {
        title: "About",
        url: "/about",
        icon: "information-circle-outline"
      },
    ]
  }
}
