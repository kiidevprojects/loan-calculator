import { Component, OnInit } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth'

import { AngularFirestore } from '@angular/fire/firestore'
import { UserService } from '../user.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

	email: string = ""
	password: string = ""
	cpassword: string = ""

	constructor(
		public fireAuth: AngularFireAuth,
		public afstore: AngularFirestore,
		public users: UserService,
		public alertController: AlertController,
		public router: Router
		) { }

	ngOnInit() {
	}

	back(){
		this.router.navigate(["/home"]);
	}

	async presentAlert(title: string, content: string) {
		const alert = await this.alertController.create({
			header: title,
			message: content,
			buttons: ['OK']
		})

		await alert.present()
	}
	// passdontmatch: string;

	// errorPassword: string;

	onRegistererror(err) {
		console.log(err);
	}

	async register() {
		const { email, password, cpassword } = this
		if(password !== cpassword) {
			//return //console.error("Passwords don't match")
			this.presentAlert('Error', 'Passwords do not match.')
		}
		if(email == "" && password !== ""){
			this.presentAlert('Error', 'Enter an email address.')
		}
		if(password == "" && email !== ""){
			this.presentAlert('Error', 'Enter a password.')
		}
		if(!email.includes("@") || !email.includes(".com")){
			this.presentAlert('Error', 'Enter a proper email format.')
		}
		// if(){
		// 	this.presentAlert('Error', 'Enter a proper email format.')
		// }

		try {
			const res = await this.fireAuth.auth.createUserWithEmailAndPassword(email, password)

			this.afstore.doc(`users/${res.user.uid}`).set({
				email
			})

			this.users.setUser({
				email,
				uid: res.user.uid
			})

			this.presentAlert('Success', 'You are registered!')
			this.router.navigate(['/home'])

		} catch(err) {
			var empty = "Enter email address or password.";
			console.dir(empty)
			// this.presentAlert('Error', empty)
			console.dir(err)
			if(err.code == "auth/weak-password"){
				this.presentAlert('Error', err)
			}
			if(err.code == "auth/email-already-in-use"){
				this.presentAlert('Error', err)
			}
		}
	}

}
