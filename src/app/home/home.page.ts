import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import * as firebase from 'firebase/app';
import { Platform, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { UserService } from '../user.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  loading: any;

  email: string = ""
  password: string = ""

  user: any = {}
  constructor(public loadingController: LoadingController,
    private google:GooglePlus,
    private platform: Platform,
    private fireAuth: AngularFireAuth,
    private router: Router,
    private fb: Facebook,
    private splashScreen: SplashScreen,
    public users: UserService,
    public alertController: AlertController) {
  }

  async presentLoading(msg) {
    this.loading = await this.loadingController.create({
      message: msg
    });
    return await this.loading.present();
  }

  toRegister(){
    this.router.navigate(["/register"]);
  }

  async login() {
    let params;
    if (this.platform.is('android')) {
      params = {
        'webClientId': '791950069316-0vh9l5c4rp0ueatikuk0ots6uvskveru.apps.googleusercontent.com', // from firebase
        'offline': true
      }
    }
    else {
      params = {}
    }
    this.google.login(params)
      .then((response) => {
        const { idToken, accessToken } = response
        this.onLoginSuccess(idToken, accessToken);
      }).catch((error) => {
        console.log(error)
        // alert('error:' + JSON.stringify(error))
      });
  }
  onLoginSuccess(accessToken, accessSecret) {
    const credential = accessSecret ? firebase.auth.GoogleAuthProvider
        .credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider
            .credential(accessToken);
    this.fireAuth.auth.signInWithCredential(credential)
      .then((response) => {
        this.router.navigate(["/calculate"]);
        this.loading.dismiss();
      })

  }

  async loginFb() {
    this.fb.login(['email'])
      .then((response: FacebookLoginResponse) => {
        this.onFbLoginSuccess(response);
        console.log(response.authResponse.accessToken);
      }).catch((error) => {
        console.log(error)
        // alert('error:' + error)
      });
  }

  FacebookLoginFirebase(): Promise<any> {
    return this.fb.login(['email'])
      .then( response => {
        const facebookCredential = firebase.auth.FacebookAuthProvider
          .credential(response.authResponse.accessToken);

        firebase.auth().signInWithCredential(facebookCredential)
          .then( success => { 
            this.router.navigate(["/calculate"]);
          this.loading.dismiss();
          });

      }).catch((error) => { console.log(error) });
  }

  onFbLoginSuccess(res: FacebookLoginResponse) {
    const credential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
    this.fireAuth.auth.signInWithCredential(credential)
      .then((response) => {
        this.router.navigate(["/calculate"]);
        this.loading.dismiss();
      })
  }

  onLoginError(err) {
    console.log(err);
  }

  async ngOnInit() {
  	this.loading = await this.loadingController.create({
      message: 'Connecting ...'
    });

    this.fireAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.user = {
          uid: user.uid,
          phoneNumber: user.phoneNumber,
          photoURL: user.photoURL,
          creationTime: user.metadata.creationTime,
          lastSignInTime: user.metadata.lastSignInTime,
          isAnonymous: user.isAnonymous,
          email: user.email,
          displayName: user.displayName,
          emailVerified: user.emailVerified,
          refreshToken: user.refreshToken
        }
      }
      else {
        this.router.navigate(["/home"]);
      }
    })
  }

  async presentAlert(title: string, content: string) {
		const alert = await this.alertController.create({
			header: title,
			message: content,
			buttons: ['OK']
		})

		await alert.present()
	}

  errorLogin;

  async loginEmail() {
		const { 
      email, password } = this
      if(email == "" && password !==""){
        this.presentAlert('Error', 'Input field is empty. Enter an email address.')
      }
      if(password == "" && email !== ""){
        this.presentAlert('Error', 'Input field is empty. Enter a password.')
      }
      if(email == "" && password == ""){
        this.presentAlert('Error', 'Input fields are empty. Enter an email and password.')
      }
      if((!email.includes("@") || !email.includes(".com")) && email !== ""){
        this.presentAlert('Error', 'Invalid input. Enter a proper email format.')
      }
		try {
			// kind of a hack. 
			const res = await this.fireAuth.auth.signInWithEmailAndPassword(email, password)
			
			if(res.user) {
				this.user.setUser({
					email,
					uid: res.user.uid
				})
				this.router.navigate(['/calculate'])
      }
      // else{
      //   this.errorLogin = "Enter Login Credentals";
      // }
		
		} catch(err) {
			console.dir(err)
			if(err.code === "auth/user-not-found") {
        //console.log("User not found")
        this.presentAlert('Error', 'User not found!')
      }
      if(err.code == "auth/wrong-password"){
        this.presentAlert('Error', err)
      }
		}
	}
  
}
