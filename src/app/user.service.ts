import { Injectable } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/auth'
import { first } from 'rxjs/operators'

interface users {
	email: string,
	uid: string
}

@Injectable()
export class UserService {
	private user: users

	constructor(private fireAuth: AngularFireAuth) {

	}

	setUser(user: users) {
		this.user = user
	}

	getEmail(): string {
		return this.user.email
	}

	// reAuth(email: string, password: string) {
	// 	return this.fireAuth.auth.currentUser.reauthenticateWithCredential(auth.EmailAuthProvider.credential(email + '@codedamn.com', password))
	// }

	updatePassword(newpassword: string) {
		return this.fireAuth.auth.currentUser.updatePassword(newpassword)
	}

	updateEmail(newemail: string) {
		return this.fireAuth.auth.currentUser.updateEmail(newemail)
	}

	async isAuthenticated() {
		if(this.user) return true

		const user = await this.fireAuth.authState.pipe(first()).toPromise()

		if(user) {
			this.setUser({
				email: user.email.split('@')[0],
				uid: user.uid
			})

			return true
		}
		return false
	}

	getUID(): string {
		return this.user.uid
	}
}